// Got help from Kanos group demo. **Issue was i had my value set as item in my class App return.**

import React, { Component } from "react";
import todosList from "./todos.json";
import { v4 as uuidv4 } from 'uuid';

class App extends Component {
  state = {
    todos: todosList,
    item: ""
  };

  handleDelete = (todoId) => {
    const newTodos = this.state.todos.filter(
      todoItem => todoItem.id !== todoId
    )
    this.setState({ todos: newTodos })
  }

  handleDeleteCompleted = () => {
    const newTodos = this.state.todos.filter(
      todoItem => todoItem.completed === false
    )
    this.setState({ todos: newTodos })
  }

  handleCheckedTodo = (checkId) => {
    const newTodos = this.state.todos.map(
      todoItem => {
        if (todoItem.id === checkId) {
          todoItem.completed = !todoItem.completed
        }
        return todoItem
      }
    )
    this.setState({ todos: newTodos })
  }


  handleChange = (event) => {
    this.setState({ item: event.target.value });
  }

  handleSubmit = (event) => {
    if (event.key === "Enter") {
      event.preventDefault()
      this.handleAddTodo()
    }
  }

  handleAddTodo = () => {
    const newTodo = {
      userId: 1,
      id: uuidv4(),
      title: this.state.item,
      completed: false,
    }
    const newTodos = [...this.state.todos, newTodo]
    this.setState({
      todos: newTodos,
      item: ""
    })
  }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            type="text"
            onChange={this.handleChange}
            onKeyDown={this.handleSubmit}
            value={this.state.item}
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus />
        </header>
        <TodoList
          todos={this.state.todos}
          handleCheck={this.handleCheckedTodo}
          handleDelete={this.handleDelete} />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button onClick={this.handleDeleteCompleted} className="clear-completed">Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            onChange={() => this.props.handleCheck(this.props.id)}
            className="toggle"
            type="checkbox"
            checked={this.props.completed} />
          <label>{this.props.title}</label>
          <button
            className="destroy"
            onClick={() => this.props.handleDelete(this.props.id)}
          />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              handleCheck={this.props.handleCheck}
              handleDelete={this.props.handleDelete}
              title={todo.title}
              key={uuidv4()}
              id={todo.id}
              completed={todo.completed} />
          ))}
        </ul>
      </section>
    );
  }
}
export default App;
